import requests
import json
import time
import MySQLdb
from requests.packages.urllib3.exceptions import InsecureRequestWarning
import gtable
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
sess = requests.Session()
sess.get("https://kaspi.kz/shop/karaganda/",verify=False)
def get_json(url):
    global sess
    is_printed = False
    while True:
        try:
            response = sess.get(url,verify=False, timeout=5)
            break
        except:
            if not is_printed:
                print("waiting for timeout")
                is_printed = True
            time.sleep(5)
    return response.json()["data"]
def get_products_from_page(page_id):
    global sess
    url = "https://kaspi.kz/shop/rest/misc/desktop/search/results?q=%3Acategory%3AFurniture&page="+str(page_id)
    return get_json(url)
def get_comments(product_id):
    global sess
    url = "https://kaspi.kz/shop/rest/misc/product/"+str(product_id)+"/reviews?limit=10000&id=all&page=0"
    return get_json(url)
def get_sellers(href):
    global sess
    url = href+"/offers/?c=351010000&limit=1000&page=0"
    return get_json(url)
def get_all_products():
    data = []
    for i in range(1,100000):
        print(i)
        new_data = get_products_from_page(i)
        if new_data:
            for i in range(len(new_data)):
                new_data[i].update({"comments":get_comments(new_data[i]["id"])})
                href = new_data[i]["shopLink"]
                while True:
                    try:
                        new_data[i].update({"sellers":get_sellers(href[:href.find("/?")])})
                        break
                    except Exception as e:
                        print(e)
            data+=new_data
        else:
            break
    return data
def insert_all_products(products):
    conn = MySQLdb.connect('localhost', 'non-root', '123', 'parse_kaspi')
    cursor = conn.cursor()
    cursor.execute("DELETE FROM `products` WHERE 1")
    conn.commit()
    cursor.close()
    conn.close()
    for product in products:
        insert_product_in_db(product)
def insert_product_in_db(product):
    conn = MySQLdb.connect('localhost', 'non-root', '123', 'parse_kaspi')
    cursor = conn.cursor()
    conn.set_character_set('utf8')
    cursor.execute('SET NAMES utf8;')
    cursor.execute('SET CHARACTER SET utf8;')
    cursor.execute('SET character_set_connection=utf8;')
    query = "INSERT INTO `products`(`data_json`) VALUES ('%s')"%json.dumps(product).replace("\\",r"\\")
    # f = open("aga.txt","w")
    # f.write(query)
    # f.close()
    cursor.execute(query)
    conn.commit()
    cursor.close()
    conn.close()
a=time.time()
products = get_all_products()
insert_all_products(products)
gtable.insert_all_products(products,"18bQ0HgsK2td53DcmZ2FcMhwBk_aW5yyAudBU1aYd46s")
b=time.time()
print("Done! Time:")
print(b-a)
